import React from 'react';
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Home from './components/pages/Home';
import Services from './components/pages/Services';
import Products from './components/pages/Services';
import SignUp from './components/pages/Services';

function App() {
  return (
   <>
    <Router>
    <Navbar />
    <Switch>
      <Route path='/' exact component={Home} />
      <route path='/services' component={Services} />
      <route path='/products' component={Products} />
      <route path='/sign-up' component={SignUp} />
    </Switch>
    </Router>
</>
  );
}

export default App;
